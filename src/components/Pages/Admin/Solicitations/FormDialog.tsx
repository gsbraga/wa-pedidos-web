import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import Slide from '@material-ui/core/Slide';
import makeStyles from '@material-ui/core/styles/makeStyles';
import TextField from 'components/Shared/Fields/Text';
import Toast from 'components/Shared/Toast';
import { logError } from 'helpers/rxjs-operators/logError';
import { useFormikObservable } from 'hooks/useFormikObservable';
import ISolicitation from 'interfaces/models/solicitation';
import React, { forwardRef, Fragment, memo, useCallback } from 'react';
import { tap } from 'rxjs/operators';
import solicitationService from 'services/solicitation';
import * as yup from 'yup';

interface IProps {
  opened: boolean;
  solicitation?: ISolicitation;
  onComplete: (solicitation: ISolicitation) => void;
  onCancel: () => void;
}

const validationSchema = yup.object().shape({
  title: yup.string().required().min(3).max(50),
  description: yup.string().required().min(3).max(50),
  quantity: yup.number().required(),
  valueTotal: yup.number().required()
});

const useStyle = makeStyles({
  content: {
    width: 600,
    maxWidth: 'calc(95vw - 50px)'
  },
  heading: {
    marginTop: 20,
    marginBottom: 10
  }
});

const FormDialog = memo((props: IProps) => {
  const classes = useStyle(props);

  const formik = useFormikObservable<ISolicitation>({
    initialValues: { },
    validationSchema,
    onSubmit(model) {
      return solicitationService.save(model).pipe(
        tap(solicitation => {
          Toast.show(`${solicitation.title} foi salvo${model.id ? '' : '!'}`);
          props.onComplete(solicitation);
        }),
        logError(true)
      );
    }
  });

  const handleEnter = useCallback(() => {
    formik.setValues(props.solicitation ?? formik.initialValues, false);
  }, [formik, props.solicitation]);

  const handleExit = useCallback(() => {
    formik.resetForm();
  }, [formik]);

  return (
    <Dialog
      open={props.opened}
      disableBackdropClick
      disableEscapeKeyDown
      onEnter={handleEnter}
      onExited={handleExit}
      TransitionComponent={Transition}
    >
      {formik.isSubmitting && <LinearProgress color='primary' />}

      <form onSubmit={formik.handleSubmit}>
        <DialogTitle>{formik.values.id ? 'Editar' : 'Novo'} Pedido</DialogTitle>
        <DialogContent className={classes.content}>
          {(
            <Fragment>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12}>
                <FormControl fullWidth>
                <TextField label='Title' name='title' formik={formik} />
                </FormControl>
                 
                </Grid>
                <Grid item xs={12} sm={12}>
                <FormControl fullWidth>
                  <TextField label='Descrição' name='description' multiline rows={4} formik={formik} />
                </FormControl>
                  
                </Grid>

                <Grid item xs={6} sm={6}>
                <TextField label='Quantidade' name='quantity' type='number' formik={formik} />
                </Grid>

                <Grid item xs={6} sm={6}>
                <TextField label='Valor' name='valueTotal' type='float' formik={formik} />
                </Grid>
              </Grid>
    
            </Fragment>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onCancel}>Cancelar</Button>
          <Button color='primary' variant='contained' type='submit' disabled={formik.isSubmitting /*|| !roles*/}>
            Salvar
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
});

const Transition = memo(
  forwardRef((props: any, ref: any) => {
    return <Slide direction='up' {...props} ref={ref} />;
  })
);

export default FormDialog;
