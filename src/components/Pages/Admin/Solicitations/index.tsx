import React, { memo } from 'react';
import { Route, Switch } from 'react-router-dom';

import SolicitationListPage from './List';

const SolicitationIndexPage = memo(() => {
  return (
    <Switch>
      <Route path='/' component={SolicitationListPage} />
    </Switch>
  );
});

export default SolicitationIndexPage;
