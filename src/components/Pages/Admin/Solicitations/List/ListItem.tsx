import React, { memo, /*useCallback, useMemo, useState*/ } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import ISolicitation from 'interfaces/models/solicitation';

interface IProps {
  solicitation: ISolicitation;
  onEdit: (solicitation: ISolicitation) => void;
  onDeleteComplete: () => void;
}
const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
});

const ListItem = memo((props: IProps) => {

  const classes = useStyles();
  const { solicitation } = props;
  
  return (
    <Card key={solicitation.id} className={classes.root}>
      <CardActionArea> 
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
          {solicitation.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
          {solicitation.description}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
      <Button size="small" color="primary">
        Quantidade: {solicitation.valueTotal}
        </Button>
        <Button size="small" color="primary">
        Valor: R$ {solicitation.valueTotal}
        </Button>
      </CardActions>
    </Card>
  );
});

export default ListItem;


