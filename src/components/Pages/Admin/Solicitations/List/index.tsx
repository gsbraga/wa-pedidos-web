import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from 'components/Layout/Toolbar';
import CardLoader from 'components/Shared/CardLoader';
import EmptyAndErrorMessagesSolicitation from 'components/Shared/Pagination/EmptyAndErrorMessagesSolicitation';
import SearchField from 'components/Shared/Pagination/SearchField';
import TablePagination from 'components/Shared/Pagination/TablePagination';
import usePaginationObservable from 'hooks/usePagination';
import ISolicitation from 'interfaces/models/solicitation';
import RefreshIcon from 'mdi-react/RefreshIcon';
import React, { Fragment, memo, useCallback, useState } from 'react';
import solicitationService from 'services/solicitation';

import FormDialog from '../FormDialog';
import ListItem from './ListItem';

const SolicitationListPage = memo(() => {
  const [formOpened, setFormOpened] = useState(false);
  const [current, setCurrent] = useState<ISolicitation>();

  const [params, mergeParams, loading, data, error, , refresh] = usePaginationObservable(
    params => solicitationService.list(params),
    { orderBy: 'title', orderDirection: 'asc' },
    []
  );

  const handleCreate = useCallback(() => {
    setCurrent(null);
    setFormOpened(true);
  }, []);

  const handleEdit = useCallback((current: ISolicitation) => {
    setCurrent(current);
    setFormOpened(true);
  }, []);

  const formCallback = useCallback(
    (solicitation?: ISolicitation) => {
      setFormOpened(false);
      current ? refresh() : mergeParams({ term: solicitation.description });
    },
    [current, mergeParams, refresh]
  );

  const formCancel = useCallback(() => setFormOpened(false), []);
  const handleRefresh = useCallback(() => refresh(), [refresh]);

  const { total, results } = data || ({ total: 0, results: [] } as typeof data);

  return (
    <Fragment>
      <Toolbar title='Pedidos' />

      <Card>
        <FormDialog opened={formOpened} solicitation={current} onComplete={formCallback} onCancel={formCancel} />

        <CardLoader show={loading} />

        <CardContent>
          <Grid container justify='space-between' alignItems='center' spacing={2}>
            <Grid item xs={12} sm={6} lg={4}>
              <SearchField paginationParams={params} onChange={mergeParams} />
            </Grid>

            <Grid item xs={12} sm={'auto'}>
              <Button fullWidth variant='contained' color='primary' onClick={handleCreate}>
                Adicionar
              </Button>
            </Grid>
          </Grid>
        </CardContent>
        <IconButton disabled={loading} onClick={handleRefresh}>
          <RefreshIcon />
        </IconButton>  
       
        <Grid container justify='space-between' alignItems='center' spacing={2}>
        <EmptyAndErrorMessagesSolicitation
                error={error}
                loading={loading}
                hasData={results.length > 0}
                onTryAgain={refresh}
              />
        {results.map(solicitation => (
          <Grid key={solicitation.id}  item xs={12} sm={6} lg={4}>
            <ListItem solicitation={solicitation} onEdit={handleEdit} onDeleteComplete={refresh} />
        </Grid>
        ))}
          </Grid>       

        <TablePagination total={total} disabled={loading} paginationParams={params} onChange={mergeParams} />
      </Card>
    </Fragment>
  );
});

export default SolicitationListPage;
