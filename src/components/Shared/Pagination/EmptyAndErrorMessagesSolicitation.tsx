// import TableCell from '@material-ui/core/TableCell';
// import TableRow from '@material-ui/core/TableRow';
import AlertCircleOutlineIcon from 'mdi-react/AlertCircleOutlineIcon';
import React, { Fragment, memo } from 'react';

import ErrorMessage from '../ErrorMessage';
import IconMessage from '../IconMessage';
import { CardContent, Card } from '@material-ui/core';

interface IProps {
  error?: any;
  loading?: boolean;
  hasData: boolean;
  onTryAgain: () => void;
}

const EmptyAndErrorMessagesSolicitation = memo((props: IProps) => {
  const { error, hasData, loading, onTryAgain } = props;

  return (
    <Fragment>
      {loading && !hasData && (
        <Card>
          <CardContent className='empty'>
            Carregando...
          </CardContent>
        </Card>
      )}
      {error && !loading && (       
        <Card>
          <CardContent className='error'>
            <ErrorMessage error={error} tryAgain={onTryAgain} />
          </CardContent>
      </Card>
      )}
      {!error && !hasData && !loading && (
        <Card>
            <CardContent>
            <IconMessage icon={AlertCircleOutlineIcon} message='Nenhum item cadastrado...' />
            </CardContent>
        </Card>
      )}
    </Fragment>
  );
});

export default EmptyAndErrorMessagesSolicitation;
