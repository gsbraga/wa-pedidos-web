export default interface ISolicitation {
  id?: number;
  userId: number;
  title?: string;
  description?: string;
  valueTotal?: number;
  quantity?: number;

  createdDate?: Date;
  updatedDate?: Date;
}